package com.example.richmond.numbershapes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    class Number {
        private int number;

        public void setNumber(int number) {
            this.number = number;
        }

        public int getNumber() {
            return this.number;
        }

        public boolean isSquare() {

            double squareRoot = Math.sqrt(this.number);

            if(squareRoot == Math.floor(squareRoot)) {
                return true;
            }
            else {
                return false;
            }
        }

        public boolean isTriangular() {

            int counter = 1;
            int triangularNumber = 1;

            while(triangularNumber < this.number) {
                counter++;
                triangularNumber = triangularNumber + counter;
            }

            if(triangularNumber == this.number) {
                return true;
            }
            else {
                return false;
            }
        }
    }

    public void testNumber(View view) {

        EditText usersNumber = (EditText) findViewById(R.id.usersNumber);
        String message = "";
        Number myNumber = new Number();

        if(usersNumber.getText().toString().isEmpty()) {
            message = "Please enter a number!";
        }
        else {
            myNumber.setNumber(Integer.parseInt(usersNumber.getText().toString()));

            if(myNumber.isSquare() && myNumber.isTriangular()) {
                message = "Number is a square and a triangular number!";
            }
            else if(myNumber.isSquare()) {
                message = "Number is a square number!";
            }
            else if(myNumber.isTriangular()) {
                message = "Number is a triangular number!";
            }
            else {
                message = "Number is neither a square nor a triangular number!";
            }
        }

        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
